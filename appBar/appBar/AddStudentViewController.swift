//
//  AddStudentViewController.swift
//  appBar
//
//  Created by Etay Alony on 10/05/2022.
//

import UIKit

class AddStudentViewController: UIViewController, GetMainViewSegueProtocol {

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var idTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var addressTextfield: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func getViewContainer() -> UIView {
        return self.view;
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "saveStudentSegue") {
            let dvc = segue.destination as! StudentDetailsViewController;
            
            let nameParts = nameTextfield.text?.components(separatedBy: " ") as [String];
            
            let newId = Int(idTextfield.text ?? "") ?? 0;
            let newFirstname = nameParts[0];
            let newLastname = nameParts.count > 1 ? nameParts[1] : "";
            let newPhone = Int(phoneTextfield.text ?? "") ?? 0;
            let newAddress = addressTextfield.text!;
            
            let newStudent = Student(
                id: newId,
                firstname: newFirstname,
                lastname: newLastname,
                phoneNumber: newPhone,
                address: newAddress
            )
            
            DataModel.instance.addStudents(student: newStudent);
            dvc.student = newStudent;
        }
    }


}
