//
//  MainViewController.swift
//  appBar
//
//  Created by Etay Alony on 07/05/2022.
//

import UIKit

class MainViewController: UIViewController, GetMainViewSegueProtocol {

    @IBOutlet weak var mainScreenView: UIView!
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var aboutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        performSegue(withIdentifier: "homePageSegue", sender: self);
    }
    
    func getViewContainer() -> UIView {
        return mainScreenView;
    }

}


protocol GetMainViewSegueProtocol {
    func  getViewContainer() -> UIView;
}

class  GetMainViewSegue: UIStoryboardSegue {
    override func perform() {
        
        let sourceVc = self.source;
        let destVc = self.destination;
        
        sourceVc.addChild(destVc);
        
        if let sourceVcProtocol =  sourceVc as? GetMainViewSegueProtocol{
            let viewContainer = sourceVcProtocol.getViewContainer();
            destVc.view.frame  = viewContainer.frame;
            destVc.view.frame.origin.x = 0;
            destVc.view.frame.origin.y = 0;
            
            viewContainer.addSubview(destVc.view)
        }
    }
}
