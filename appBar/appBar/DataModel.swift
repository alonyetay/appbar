//
//  DataModel.swift
//  appBar
//
//  Created by Etay Alony on 08/05/2022.
//

import Foundation

class DataModel {
    static let instance = DataModel();
    
    var students: [Student];
    
    private init() {
        let NUM_OF_STUDENTS: Int = 10;
        self.students = [Student]();
        
        for index in 0...NUM_OF_STUDENTS {
            let currStudent = Student(id: index, firstname: "first\(index)", lastname: "last\(index)", phoneNumber: 0545414900+index, address: "address\(index)");
            
            self.students.append(currStudent);
        }
    }
    
    public func addStudents(student: Student) {
        self.students.append(student);
    }
}

class Student {
    var id: Int;
    var firstname: String;
    var lastname: String;
    var phoneNumber: Int;
    var address: String;
    
    init(id: Int, firstname: String, lastname: String, phoneNumber: Int, address: String) {
        self.id = id;
        self.firstname = firstname;
        self.lastname = lastname;
        self.phoneNumber = phoneNumber;
        self.address = address;
    }
}
