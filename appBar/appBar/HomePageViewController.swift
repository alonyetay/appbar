//
//  HomePageViewController.swift
//  appBar
//
//  Created by Etay Alony on 07/05/2022.
//

import UIKit

class HomePageViewController: UITableViewController{

    private var students: [Student] = DataModel.instance.students;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for: indexPath) as! StudentTableViewCell;
        
        let currStudent = students[indexPath.row];
        cell.studentName = "\(currStudent.firstname) \(currStudent.lastname)";
        cell.studentId =  "ID: \(String(currStudent.id))";
        
        return cell;
    }
    
    var selectedRowIndex: Int? = nil;
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRowIndex = students[indexPath.row].id;
        performSegue(withIdentifier: "openStudentDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "openStudentDetails") {
            let dvc = segue.destination as! StudentDetailsViewController;
            let currStudent = students[selectedRowIndex ?? 0]
            dvc.student = currStudent;
        }
    }
}
