//
//  StudentDetailsViewController.swift
//  appBar
//
//  Created by Etay Alony on 09/05/2022.
//

import UIKit

class StudentDetailsViewController: UIViewController {
    
    var student: Student? {
        didSet {
            if (nameLabel != nil) {
                nameLabel.text = "\(student?.firstname) \(student?.lastname)";
            }
            
            if (idLabel != nil) {
                idLabel.text = String(student?.id ?? 0);
            }
            
            if (phoneLabel != nil) {
                phoneLabel.text = String(student?.phoneNumber ?? 0);
            }
            
            if (addressLabel != nil) {
                addressLabel.text = student?.address;
            }
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let student = student {
            nameLabel.text = "\(student.firstname) \(student.lastname)";
            idLabel.text = String(student.id);
            phoneLabel.text = String(student.phoneNumber);
            addressLabel.text = student.address;
        }
    }
}
