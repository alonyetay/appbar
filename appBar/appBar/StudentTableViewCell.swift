//
//  StudentTableViewCell.swift
//  appBar
//
//  Created by Etay Alony on 08/05/2022.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var studentIdLabel: UILabel!
    
    var studentName = "" {
        didSet{
            if (studentNameLabel != nil) {
                studentNameLabel.text = studentName;
            }
        }
    }
    
    var studentId = "" {
        didSet{
            if (studentIdLabel != nil) {
                studentIdLabel.text = studentId;
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        studentIdLabel.text = studentId;
        studentNameLabel.text = studentName;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
